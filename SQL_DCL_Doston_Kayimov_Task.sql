--Note: In the beginning af this course, I couldn't restore dvdrental database. After many attempts, it was stored once.
--But then, it didn't work, I couldn't restore dvdrental database again. I couldn't find the reason.
--I tried other versions of postgreSQL and corrected binary path in preferences but no results.
--So now, i've been working in the same dvdrental database since the first practice. Maybe that's why my codes aren't accurate sometimes.
--Because dvdrevtal database has been tempered many times in pgAdmin/DBeaver since the first practice.


--creating a new user with the username "rentaluser" and the password "rentalpassword"
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

--granting "rentaluser" SELECT permission for the "customer" table
GRANT SELECT ON customer TO rentaluser;
SELECT * FROM customer;

--creating a new user group called "rental" and adding "rentaluser" to the group
CREATE GROUP rental;
GRANT rental TO rentaluser;

--granting the "rental" group INSERT and UPDATE permissions for the "rental" table
GRANT INSERT, UPDATE ON rental TO rental;
INSERT INTO rental (rental_date, inventory_id, customer_id, return_date) VALUES ('2022-01-01', 1, 1, '2022-01-02');
UPDATE rental SET return_date = '2022-01-03' WHERE rental_id = 1;

--revoking the "rental" group's INSERT permission for the "rental" table
REVOKE INSERT ON rental FROM rental;

--creating a personalized role for an existing customer in the dvd_rental database
CREATE ROLE client_Doston_Kayimov;
GRANT SELECT ON rental TO client_Doston_Kayimov;
GRANT SELECT ON payment TO client_Doston_Kayimov;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO client_Doston_Kayimov;
GRANT client_Doston_Kayimov TO rentaluser;

--verifying that the user with the personalized role sees only their own data
SET ROLE client_Doston_Kayimov;
SELECT * FROM rental WHERE customer_id = 1;
SELECT * FROM payment WHERE customer_id = 1;